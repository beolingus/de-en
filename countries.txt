Afghanistan {n} »AF« [geogr.] :: Afghanistan
Albanien {n} »AL« [geogr.] :: Albania
Algerien {n} »DZ« [geogr.] :: Algeria
Amerikanisch-Samoa {n} »AS« [geogr.] :: American Samoa
Amerikanisch-Ozeanien {n} »UM« [geogr.] :: United States Minor Outlying Islands
Andorra {n} »AD« (Kfz: »AND«) [geogr.] :: Andorra
Angola {n} »AO« [geogr.] :: Angola
Anguilla {n} »AI« [geogr.] :: Anguilla
# Antarktis {f} »AQ« [geogr.] :: Antarctica
Antigua und Barbuda {n} »AG« [geogr.] :: Antigua and Barbuda
Argentinien {n} »AR« (Kfz: »RA«) [geogr.] :: Argentina
Armenien {n} »AM« [geogr.] :: Armenia
Aruba {n} »AW« [geogr.] :: Aruba
Australien {n} »AU« (Kfz: »AUS«) [geogr.] :: Australia
Österreich {n} »AT« (Kfz: »A«) [geogr.] :: Austria
 Ostösterreich {n} :: Eastern Austria; East Austria
 Westösterreich {n} :: Western Austria; West Austria
Aserbaidschan {n} »AZ« [geogr.] :: Azerbaijan
Bahamas {pl} »BS« [geogr.] :: Bahamas
 auf den Bahamas :: in the Bahamas
Bahrain {n} »BH« (Kfz: »BRN«) [geogr.] :: Bahrain
Bangladesh {n} »BD« [geogr.] :: Bangladesh
 aus Bangladesh :: Bangladeshi
Barbados {n} »BB« (Kfz: »BDS«) [geogr.] :: Barbados
Belarus; Belorussland; Weißrussland; Belorußland [alt]; Weißrußland [alt] {n} »BY« [geogr.] :: Belarus
Belgien {n} »BE« (Kfz: »B«) [geogr.] :: Belgium
Belize {n} »BZ« (Kfz: »BH«) [geogr.] :: Belize
Benin {n} »BJ« (Kfz: »DY«) [geogr.] :: Benin
Bermuda {n} »BM« [geogr.] :: Bermuda
Bhutan {n} »BT« [geogr.] :: Bhutan
Bolivien {n} »BO« [geogr.] :: Bolivia
Bosnien und Herzegowina; Bosnien-Herzegowina {n} »BA« (Kfz: »BIH«) [geogr.] :: Bosnia and Herzegovina; Bosnia-Herzegovina
Botsuana {n} »BW« [geogr.] :: Botswana
Bouvet-Insel {f} »BV« [geogr.] :: Bouvet Island
Brasilien {n} »BR« [geogr.] :: Brazil
Britisches Territorium {n} im Indischen Ozean »IO« [geogr.] :: British Indian Ocean Territory
Brunei-Darussalam; Brunei {n} »BN« (Kfz: »BRU«) [geogr.] :: Brunei-Darussalam; Brunei
Bulgarien {n} »BG« [geogr.] :: Bulgaria
Burkina Faso {n} (Obervolta) »BF« [geogr.] :: Burkina Faso (Upper Volta)
Burundi {n} »BI« (Kfz: »RU«) [geogr.] :: Burundi
Kambodscha {n} »KH« (Kfz: »K«); Kampuchea {n} [hist.] [geogr.] :: Cambodia
Kamerun {n} »CM« (Kfz: »CAM«) [geogr.] :: Cameroon
Kanada {n} »CA« (Kfz: »CDN«) [geogr.] :: Canada
Kap Verde {n} »CV« [geogr.] :: Cape Verde
Kaimaninseln {pl} »KY« [geogr.] :: Cayman Islands
Zentralafrikanische Republik {f} »CF« (Kfz: »RCA«) [geogr.] :: Central African Republic
Tschad {m} »TD« (Kfz: »TCH«) [geogr.] :: Chad
Chile {n} »CL« (Kfz: »RCH«) [geogr.] :: Chile
China {n}; Volksrepublik China {f} »CN« (Kfz: »RC«) [geogr.] :: China; People’s Republic of China
 Festlandchina {n} :: mainland China; Chinese mainland
Weihnachtsinsel {f} »CX« [geogr.] :: Christmas Island
Cocosinseln {pl} »CC« [geogr.] :: Cocos (Keeling) Islands
Kolumbien {n} »CO« [geogr.] :: Colombia
Komoren {pl} »KM« [geogr.] :: Comoros
Kongo {m} (Republik Kongo) »CG« (Kfz: »RCB«) [geogr.] :: Congo (Republic of the Congo)
Kongo {m} (Demokratische Republik Kongo); Zaire »CD« (Kfz: »ZRE«) [geogr.] :: Congo (Democratic Republic of the Congo); Zaire
Cook Inseln {pl} »CK« [geogr.] :: Cook Islands
Costa Rica {n} »CR« [geogr.] :: Costa Rica
Cote d’Ivoire (Elfenbeinküste) {f} »CI« :: Cote d’Ivoire (Ivory Coast)
Kroatien {n} »HR« [geogr.] :: Croatia (local name: Hrvatska)
Kuba {n} »CU« [geogr.] :: Cuba
Zypern {n} »CY« [geogr.] :: Cyprus
 Nordzypern :: Northern Cyprus
Tschechien {n} (Tschechische Republik) »CZ« [geogr.] <Tschechei> :: Czechia (Czech Republic)
Dänemark {n} »DK« [geogr.] :: Denmark
Dschibuti {n} »DJ« [geogr.] :: Djibouti
Dominica {n} »DM« (Kfz: »WD«) [geogr.] :: Dominica
Dominikanische Republik {f} »DO« (Kfz: »DOM«) [geogr.] :: Dominican Republic
Ekuador {n} »EC« [geogr.] :: Ecuador
Ägypten {n} »EG« [geogr.] :: Egypt
El Salvador {n} »SV« (Kfz: »ES«) [geogr.] :: El Salvador
Äquatorialguinea {n} »GQ« [geogr.] :: Equatorial Guinea
Eritrea {n} »ER« [geogr.] :: Eritrea
Estland {n} »EE« (Kfz: »EST«) [geogr.] :: Estonia
Äthiopien {n} »ET« (Kfz: »ETH«) [geogr.] :: Ethiopia
Falkland-Inseln {pl}; Malwinen {pl} »FK« [geogr.] :: Falkland Islands; Malvinas
Färöer {n} »FO« [geogr.] :: Faroe islands
Fidschi {n} »FJ« (Kfz: »FJI«) [geogr.] :: Fiji
Finnland {n} »FI« (Kfz: »FIN«) [geogr.] :: Finland
Frankreich {n} »FR« (Kfz: »F«) [geogr.] :: France
 Nordfrankreich {n} :: Northern France; North France
 Südfrankreich {n} :: Southern France; South France
Metropolitan-Frankreich {n} »FX« [geogr.] :: France, Metropolitan
Französisch-Guayana {n} »GF« [geogr.] :: French Guiana
Französisch-Polynesien {n} »PF« [geogr.] :: French Polynesia
Französische Südpolar-Territorien {pl} »TF« [geogr.] :: French Southern Territories
Gabun {n} »GA« (Kfz: »G«) [geogr.] :: Gabon
Gambia {n} »GM« (Kfz: »WAG«) [geogr.] :: Gambia
 in Gambia :: in the Gambia
Georgien {n} »GE« [geogr.] :: Georgia
Deutschland {n} »DE« (Kfz: »D«) [geogr.] :: Germany
 Norddeutschland {n} :: Northern Germany; North Germany
 Ostdeutschland {n} :: Eastern Germany; East Germany
 Süddeutschland {n} :: Southern Germany; South Germany
 Westdeutschland {n} :: Western Germany; West Germany
 Nachkriegsdeutschland {n} [hist.] :: post-war Germany
Bundesrepublik {f} Deutschland »BRD« [geogr.] [pol.] :: Federal Republic of Germany »FRG«
Ghana {n} »GH« [geogr.] :: Ghana
Gibraltar {n} »GI« [geogr.] :: Gibraltar
Griechenland {n} »GR« [geogr.] :: Greece
Grönland {n} »GL« [geogr.] :: Greenland
Grenada {n} »GD« (Kfz: »WG«) [geogr.] :: Grenada
Guadeloupe {n} »GP« [geogr.] :: Guadeloupe
Guam {n} »GU« [geogr.] :: Guam
Guatemala {n} »GT« (Kfz: »GCA«) [geogr.] :: Guatemala
Guinea {n} »GN« (Kfz: »RG«) [geogr.] :: Guinea
Guinea-Bissau {n} »GW« [geogr.] :: Guinea-Bissau
Guyana {n} »GY« (Kfz: »GUY«) [geogr.] :: Guyana
Haiti {n} »HT« (Kfz: »RH«) [geogr.] :: Haiti
Heard und Mc Donald-Inseln {pl} »HM« [geogr.] :: Heard and Mc Donald Islands
Vatikan {m} »VA« (Kfz: »V«) [geogr.] :: Vatican
Vatikanstadt {f} [geogr.] :: Vatican City
Honduras {n} »HN« [geogr.] :: Honduras
Hongkong {n} »HK« (Sonderverwaltungsgebiet in China) [geogr.] :: Hong Kong (special administrative region in China)
Ungarn {n} »HU« (Kfz: »H«) [geogr.] :: Hungary
Island {n} »IS« [geogr.] :: Iceland
Indien {n} »IN« (Kfz: »IND«) [geogr.] :: India
Indonesien {n} »ID« (Kfz: »RI«) [geogr.] :: Indonesia
Iran {m} »IR« [geogr.] :: Iran
Irak {m} »IQ« (Kfz: »IRQ«) [geogr.] :: Iraq
 im Irak; in Irak [Dt.] :: in Iraq
 Nordirak {m} :: Northern Iraq
 Südirak {m} :: Southern Iraq
 Zentralirak {m} :: central Iraq
Irland {n} »IE« (Kfz: »IRL«) [geogr.] :: Ireland; Eire [Ir.]
 die Republik Irland [pol.] :: the Republic of Ireland; Eire [Br.]
Israel {n} »IL« [geogr.] :: Israel
Italien {n} »IT« (Kfz: »I«) [geogr.] :: Italy
 Norditalien {n}; Oberitalien {n} :: Northern Italy; North Italy
 Süditalien {n}; Unteritalien {n} :: Southern Italy; South Italy
Jamaika {n} »JM« (Kfz: »JA«) [geogr.] :: Jamaica
Japan {n} »JP« (Kfz: »J«) [geogr.] <Nippon> <Nihon> :: Japan
Jordanien {n} »JO« (Kfz: »HKJ«) [geogr.] :: Jordan
Kasachstan {n} »KZ« [geogr.] :: Kazakhstan
Kenia {n} »KE« (Kfz: »EAK«) [geogr.] :: Kenya
Kiribati {n} »KI« [geogr.] :: Kiribati
Nordkorea {n} (Demokratische Volksrepublik Korea) »KP« [geogr.] :: North Korea (Democratic People’s Republic of Korea)
Südkorea {n} (Republik Korea) »KR« (Kfz: »ROK«) [geogr.] :: South Korea (Republic of Korea)
Kuweit {n}; Kuwait {n} »KW« (Kfz: »KWT«) [geogr.] :: Kuwait
Kirgisistan {n}; Kirgistan {n}; Kirgisien {n} »KG« (Kfz: »KS«) [geogr.] :: Kyrgyzstan; Kirghizia; Kirgizia
Laos {n} (Demokratische Volksrepublik Laos) »LA« (Kfz: »LAO«) [geogr.] :: Laos (Lao People’s Democratic Republic)
Lettland {n} »LV« [geogr.] :: Latvia
Libanon {m} »LB« (Kfz: »RL«) [geogr.] :: Lebanon
 im Libanon; in Libanon [Dt.] :: in Lebanon
Lesotho {n} »LS« [geogr.] :: Lesotho
Liberia {n} »LR« [geogr.] :: Liberia
Libyen {n} (Libysch-arabische Dschamahirija) »LY« (Kfz: »LAR«) [geogr.] :: Libya (Lybian-Arab Jamahiriya)
Liechtenstein {n} (Fürstentum Liechtenstein) »LI« (Kfz: »FL«) [geogr.] <!> :: Liechtenstein (Principality of Liechtenstein)
Litauen {n} »LT« [geogr.] :: Lithuania
Luxemburg {n} »LU« (Kfz: »L«) [geogr.] :: Luxemburg; Luxembourg
Macau {n} »MO« [geogr.] :: Macau
Madagaskar {n} »MG« (Kfz: »RM«) [geogr.] :: Madagascar
Malawi {n} »MW« [geogr.] :: Malawi
Malaysia {n} »MY« (Kfz: »MAL«) [geogr.] :: Malaysia
Malediven {pl} »MV« [geogr.] :: Maldives; Maldive Islands
Mali {n} »ML« (Kfz: »RMM«) [geogr.] :: Mali
Malta {n} »MT« (Kfz: »M«) [geogr.] :: Malta
Marshallinseln {pl} »MH« [geogr.] :: Marshall Islands
Martinique {n} »MQ« [geogr.] :: Martinique
Mauretanien {n} »MR« (Kfz: »RIM«) [geogr.] :: Mauritania
Mauritius {n} »MU« (Kfz: »MS«) [geogr.] :: Mauritius
Mayotte {n} »YT« [geogr.] :: Mayotte
Mexiko {n} »MX« (Kfz: »MEX«) [geogr.] :: Mexico
Mikronesien {n} »FM« [geogr.] :: Micronesia, Federated States of
Moldawien {n} »MD« [geogr.] :: Moldova, Republic of
Monaco {n} »MC« [geogr.] :: Monaco
Mongolei {f} »MN« (Kfz: »MGL«) [geogr.] :: Mongolia
Montenegro {n} »ME« (Kfz: »MNE«) [geogr.] :: Montenegro
Montserrat {n} »MS« [geogr.] :: Montserrat
Marokko {n} »MA« [geogr.] :: Morocco
Mosambik {n} »MZ« (Kfz: »MOC«) [geogr.] :: Mozambique
Myanmar {n} (Birma) »MM« (Kfz: »BUR«) [geogr.] :: Myanmar (Burma)
Namibia {n} »NA« (Kfz: »NAM«) [geogr.] :: Namibia
Nauru {n} »NR« (Kfz: »NAU«) [geogr.] :: Nauru
Nepal {n} »NP« [geogr.] :: Nepal
die Niederlande {f} »NL« [geogr.] :: the Netherlands
Niederländische Antillen {pl} »AN« [geogr.] :: Netherlands Antilles
Neukaledonien {n} »NC« [geogr.] :: New Caledonia
Neuseeland {n} »NZ« [geogr.] :: New Zealand
Nikaragua {n}; Nicaragua {n} »NI« (Kfz: »NIC«) [geogr.] :: Nicaragua
Niger {m} »NE« (Kfz: »RN«) [geogr.] :: Niger
Nigeria {n} »NG« (Kfz: »WAN«) [geogr.] :: Nigeria
Niue {n} »NU« [geogr.] :: Niue
Norfolkinsel {f} »NF« [geogr.] :: Norfolk Island
Nördliche Marianen-Inseln {pl} »MP« [geogr.] :: Northern Mariana Islands
Norwegen {n} »NO« (Kfz: »N«) [geogr.] :: Norway
Oman {m} »OM« [geogr.] :: Oman
Pakistan {n} »PK« [geogr.] :: Pakistan
Palau {n} »PW« [geogr.] :: Palau
Palästina {n} »PS« [geogr.] :: Palestine
Panama {n} »PA« [geogr.] :: Panama
Papua-Neuguinea {n} »PG« (Kfz: »PNG«) [geogr.] :: Papua New Guinea
Paraguay {n} »PY« [geogr.] :: Paraguay
Peru {n} »PE« [geogr.] :: Peru
Philippinen {n} »PH« (Kfz: »RP«) [geogr.] :: Philippines
 auf den Philippinen :: in the Philippines
Pitcairninseln {pl} »PN« [geogr.] :: Pitcairn
Polen {n} »PL« [geogr.] :: Poland
Portugal {n} »PT« (Kfz: »P«) [geogr.] :: Portugal
Puerto Rico {n} »PR« [geogr.] :: Puerto Rico
Katar {n} »QA« (Kfz: »Q«) [geogr.] :: Qatar
Réunion {n} »RE« [geogr.] :: Reunion
Rumänien {n} »RO« [geogr.] :: Romania, Rumania
Russland {n}; Rußland [alt] {n} (Russische Föderation) »RU« (Kfz: »RUS«) [geogr.] :: Russia (Russian Federation)
Ruanda {n} »RW« (Kfz: »RWA«) [geogr.] :: Rwanda
St. Kitts und Nevis »KN« [geogr.] :: Saint Kitts and Nevis
St. Lucia {n} »LC« (Kfz: »WL«) [geogr.] :: Saint Lucia
St. Vincent {n} und die Grenadinen »VC« (Kfz: »WV«) [geogr.] :: Saint Vincent and the Grenadines
Salomonen {pl}; Salomoninseln {pl} »SB« [geogr.] :: Solomon Islands
Samoa {n}; Unabhängiger Staat Samoa; Westsamoa {n} »WS« [geogr.] :: Samoa; Independent State of Samoa
San Marino {n} »SM« (Kfz: »RSM«) [geogr.] :: San Marino
São Tomé und Príncipe {n}; Sao Tome und Principe {n}; Sankt Thomas und Prinzeninsel [selten] »ST« [geogr.] :: São Tomé and Príncipe; Sao Tome and Principe; Saint Thomas and Prince <Sao Thome and Principe>
Saudi-Arabien {n} »SA« [geogr.] :: Saudi Arabia
Senegal {m} »SN« [geogr.] :: Senegal
 in Senegal; im Senegal :: in Senegal
Serbien {n} »RS« (Kfz: »SRB«) [geogr.] :: Serbia
 Altserbien {n} [pol.] :: Old Serbia
 Großserbien {n} [pol.] [hist.] :: Greater Serbia
Seychellen {pl} »SC« (Kfz: »SY«) [geogr.] :: Seychelles
Sierra Leone {n} »SL« (Kfz: »WAL«) [geogr.] :: Sierra Leone
Singapur {n} »SG« (Kfz: »SGP«) [geogr.] :: Singapore
Slowakei {f} »SK« [geogr.] :: Slovakia
Slowakische Republik [geogr.] :: Slovak Republic
Slowenien {n} »SI« (Kfz: »SLO«) [geogr.] :: Slovenia
Somalia {n} »SO« [geogr.] :: Somalia
Spanien {n} »ES« (Kfz: »E«) [geogr.] :: Spain
 Südspanien {n} :: southern Spain; South of Spain
Südafrika {n} »ZA« [geogr.] :: South Africa
Süd-Georgien {n} und südliche Sandwichinseln »GS« [geogr.] :: South Georgia and the South Sandwich Islands
Sri Lanka {n} (Ceylon) »LK« (Kfz: »CL«) [geogr.] :: Sri Lanka (Ceylon)
St. Helena {n} »SH« [geogr.] :: St. Helena
St. Pierre und Miquelon {n} »PM« [geogr.] :: St. Pierre and Miquelon
Nordmazedonien {n}; Mazedonien {n} [ugs.] »MK« [geogr.] :: North Macedonia; Macedonia [coll.]
Nordsudan {m} »SD« (Kfz: »SUD«) [geogr.] :: North Sudan
Südsudan {m} (Kfz: »SSD«) [geogr.] :: South Sudan
Surinam {n} »SR« (Kfz: »SME«) [geogr.] :: Suriname
Svalbard {n} und Jan Mayen Insel »SJ« [geogr.] :: Svalbard and Jan Mayen Islands
Swasiland {n} »SZ« (Kfz: »SD«) [geogr.] :: Swaziland
Schweden {n} »SE« (Kfz: »S«) [geogr.] :: Sweden
Schweiz {f} »CH« [geogr.] :: Switzerland
 Ostschweiz {f} :: Eastern Switzerland; East Switzerland
 Westschweiz {f} :: Western Switzerland; West Switzerland
 Zentralschweiz {f} :: Central Switzerland
Syrien {n} (Syrische Arabische Republik) »SY« (Kfz: »SYR«) [geogr.] :: Syria (Syrian Arab Republic)
Taiwan {n} »TW«; Nationalchina {n} [hist.] [geogr.] :: Taiwan
Tadschikistan {n} »TJ« [geogr.] :: Tajikistan; Tadzhikistan
Tansania {n} »TZ« (Kfz: »EAT«) [geogr.] :: Tanzania
Thailand {n} »TH« (Kfz: »T«) [geogr.] :: Thailand
Timor-Leste {n} (Osttimor) »TL« [geogr.] :: Timor-Leste (East Timor)
Westtimor {n}; Timor Barat {n} [geogr.] :: West Timor; Timor Barat
Togo {n} »TG« [geogr.] :: Togo
Tokelau {n} »TK« [geogr.] :: Tokelau
Tonga {n} »TO« [geogr.] :: Tonga
Trinidad und Tobago {n} »TT« [geogr.] :: Trinidad and Tobago
Tunesien {n} »TN« [geogr.] :: Tunisia
Türkei {f} »TR« [geogr.] :: Turkey
Turkmenistan {n}; Turkmenien {n} »TM« [geogr.] :: Turkmenistan; Turkmenia
Turks- und Caicosinseln {pl} »TC« [geogr.] :: Turks and Caicos Islands
Tuvalu {n} »TV« [geogr.] :: Tuvalu
Uganda {n} »UG« (Kfz: »EAU«) [geogr.] :: Uganda
Ukraine {f} »UA« [geogr.] :: Ukraine; Ukrayina
 Ostukraine {f} :: eastern Ukraine
 Westukraine {f} :: western Ukraine
 in der Ukraine :: in the Ukraine; in Ukraine
Uruguay {n} »UY« (Kfz: »ROU«) [geogr.] :: Uruguay
Usbekistan {n} »UZ« (Kfz: »ZU«) [geogr.] :: Uzbekistan
Vereinigte Arabische Emirate {pl} »AE« (Kfz: »UAE«) [geogr.] :: United Arab Emirates
 in den Vereinigten Arabischen Emiraten :: in the United Arab Emirates
Großbritannien {n} (Vereinigtes Königreich von Großbritannien und Nordirland) »GB« [geogr.] :: Great Britain (United Kingdom of Great Britain and Northern Ireland »UK«)
 die Heimat Großbritannien :: Blighty [humor.]
Venezuela {n} »VE« (Kfz: »YV«) [geogr.] :: Venezuela
Vereinigte Staaten {pl} von Amerika »USA« »US« (Kfz: »USA«) [geogr.] :: United States of America »USA«
 Vereinigte Staaten :: United States »US«
Vanuatu {n} »VU« [geogr.] :: Vanuatu
Vietnam {n} »VN« [geogr.] :: Viet Nam; Vietnam
 Demokratische Republik Vietnam »DRV«; Nordvietname {n} [hist.] :: Democratic Republic of Vietnam; North Vietnam
 Südvietnam {n} [hist.] :: South Vietnam
Britische Jungferninseln {pl} »VG« [geogr.] :: British Virgin Islands
US-Jungferninseln {pl} »VI« [geogr.] :: U.S. Virgin Islands
Wallis {n} und Futuna Inseln »WF« [geogr.] :: Wallis and Futuna Islands
Westsahara {f} »EH« [geogr.] :: Western Sahara
Jemen {m} »YE« (Kfz: »YAR«) [geogr.] :: Yemen
Jugoslawien {n} »YU« [geogr.] [hist.] :: Yugoslavia
Sambia {n} »ZM« (Kfz: »RNR«) [geogr.] :: Zambia
Simbabwe {n} »ZW« [geogr.] :: Zimbabwe
