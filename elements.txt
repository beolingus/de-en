Actinium {n} »Ac« [chem.] :: actinium
Aluminium {n} »Al« [chem.] :: aluminum; aluminium [Br.]
Amerizium {n} »Am« [chem.] :: americium
Antimon {n} »Sb« [chem.] :: antimony
Argon {n} »Ar« [chem.] :: argon
Arsen {n} »As« [chem.] :: arsenic
Astat {n} »At« [chem.] :: astatine
Barium {n} »Ba« [chem.] :: barium
Berkelium {n} »Bk« [chem.] :: berkelium
Beryllium {n} »Be« [chem.] :: beryllium
Bismut {n} (Wismut) »Bi« [chem.] :: bismuth
Blei {n} [chem.] »Pb« :: lead #BE:lead# <plumbum>
 Abfallblei {n} :: scrap lead
 Elektrolytblei {n} :: electrolytic lead
 gediegenes Blei :: native lead
 Seigerblei {n} :: liquation lead
 Blei mit dem Hammer formen :: to boss lead
Bor {n} »B« [chem.] :: boron
Brom {n} »Br« [chem.] :: bromine
Cadmium {n} (Kadmium) »Cd« [chem.] :: cadmium
Caesium {n}; Cesium {n} (Zäsium) »Cs« [chem.] :: caesium [Br.]; cesium [Am.]
 Radiocäsium {n} (Cs-137) :: radiocaesium [Br.]; radiocesium [Am.] (Cs-137)
Calcium {n} (Kalzium) »Ca« [chem.] :: calcium
Californium {n} (Kalifornium) »Cf« [chem.] :: californium
Cer {n} (Zer; Zerium [veraltet]; Cerium [selten]) »Ce« [chem.] :: cerium
Chlor {n} »Cl« [chem.] :: chlorine
Chrom {n}; Chromium {n} »Cr« [chem.] :: chromium
Curium {n} »Cm« [chem.] :: curium
Dubnium {n} »Db« (Hahnium »Ha«; Nielsbohrium »Ns«; Unnilpentium »Unp«) [chem.] :: dubnium; hahnium [obs.] (nielsbohrium; unnilpentium)
Dysprosium {n} »Dy« [chem.] :: dysprosium
Einsteinium {n} »Es« [chem.] :: einsteinium
Eisen {n} »Fe« [chem.] :: iron
Erbium {n} »Er« [chem.] :: erbium
Europium {n} »Eu« [chem.] :: europium
Fermium {n} »Fm« [chem.] :: fermium
Flerovium {n} »Fl« [chem.] :: flerovium
Fluor {n} »F« [chem.] :: fluorine
Francium (Franzium) {n} »Fr« [chem.] :: francium
Gadolinium {n} »Gd« [chem.] :: gadolinium
Gallium {n} »Ga« [chem.] :: gallium
Germanium {n} »Ge« [chem.] :: germanium
Gold {n} »Au« [chem.] :: gold
Hafnium {n} »Hf« [chem.] :: hafnium; celtium [obs.]
Helium {n} »He« [chem.] :: helium
Holmium {n} »Ho« [chem.] :: holmium
Indium {n} »In« [chem.] :: indium
Jod {n}; Iod {n} [geh.] »I« [chem.] :: iodine »I«
 radioaktives Jod; Radiojod; Jod-131 :: radioactive iodine; radio-iodine; iodine 131
Iridium {n} »Ir« [chem.] :: iridium
Kalium {n} »K« [chem.] :: potassium
Kobalt {n}; Cobalt {n} »Co« [chem.] :: cobalt
Kohlenstoff {m} »C« [chem.] :: carbon
 gebundener Kohlenstoff :: fixed carbon
 gelöster organischer Kohlenstoff :: dissolved organic carbon »DOC«
 gesamter Kohlenstoff(gehalt) <!> :: total carbon »TC«
 gesamter anorganischer Kohlenstoffgehalt; gesamter anorganisch gebundener Kohlenstoff [envir.] :: total inorganic carbon »TIC«
 graphitischer Kohlenstoff :: graphitic carbon
 organischer Kohlenstoff :: organic carbon
 partikulärer organischer Kohlenstoff(gehalt) <!> :: particulate organic carbon »POC«
Krypton {n} »Kr« [chem.] :: krypton
Kupfer {n} [chem.] »Cu« :: copper #BE:copper#
 sauerstofffrei gemachtes Kupfer :: deoxidized copper
 Blastenkupfer {n}; Blisterkupfer {n} :: blister copper
Lanthan {n} »La« [chem.] :: lanthanum
Lawrencium {n} »Lr« [chem.] :: lawrencium
Lithium {n} »Li« [chem.] :: lithium
Livermorium {n} »Lv« [chem.] :: livermorium
Lutetium {n} »Lu« [chem.] :: lutetium
Magnesium {n} »Mg« [chem.] :: magnesium
Mangan {n} »Mn« [chem.] :: manganese
Mendelevium {n} »Md« [chem.] :: mendelevium
Moscovium {n} »Mc« [chem.] :: moscovium
Molybdän {n} »Mo« [chem.] :: molybdenum
Natrium {n} »Na« [chem.] :: sodium
Neodym {n} »Nd« [chem.] :: neodymium
Neon {n} »Ne« [chem.] :: neon
Neptunium {n} »Np« [chem.] :: neptunium
Nickel {n} »Ni« [chem.] :: nickel
Nihonium {n} »Nh« [chem.] :: nihonium
Niobium {n} (Niob) »Nb« [chem.] :: niobium
Nobelium {n} »No« [chem.] :: nobelium
Oganesson {n} »Og« [chem.] :: oganesson
Osmium {n} »Os« [chem.] :: osmium
Palladium {n} »Pd« [chem.] :: palladium
Phosphor {m} »P« [chem.] :: phosphorus
 weißer/gelber/farbloser Phosphor :: white phosphorus
 roter Phosphor :: red phosphorus
 violetter Phosphor :: violet phosphorus
 schwarzer Phosphor; Hittorf’scher Phosphor :: black phosphorus
Platin {n} »Pt« [chem.] :: platinum
 Knallplatin {n} :: fulminating platin
Plutonium {n} »Pu« [chem.] :: plutonium
 abgetrenntes Plutonium :: separated plutonium
 waffentaugliches Plutonium :: weapons-grade plutonium
Polonium {n} »Po« [chem.] :: polonium
Praseodym {n} »Pr« [chem.] :: praseodymium
Promethium {n} »Pm« [chem.] :: promethium
Protactinium {n} »Pa« [chem.] :: protactinium
Quecksilber {n} [chem.] »Hg« :: mercury; quicksilver
Radium {n} »Ra« [chem.] :: radium
Radon {n} »Rn« [chem.] :: radon
Rhenium {n} »Re« [chem.] :: rhenium
Rhodium {n} »Rh« [chem.] :: rhodium
Rubidium {n} »Rb« [chem.] :: rubidium
Ruthenium {n} »Ru« [chem.] :: ruthenium
Rutherfordium {n} »Rf« (Kurtschatovium »Ku«; Unnilquadium »Unq«) [chem.] :: rutherfordium (kurchatovium; unnilquadium)
Samarium {n} »Sm« [chem.] :: samarium
Sauerstoff {m} »O« [chem.] :: oxygen
 atmosphärischer Sauerstoff; Sauerstoff der Luft :: atmospheric oxygen
 flüssiger Sauerstoff :: liquid oxygen; LOX
 gelöster Sauerstoff :: dissolved oxygen »DO«; water oxygen
 molekularer Sauerstoff :: molecular oxygen
 verfügbarer Sauerstoff :: available oxygen
 Sauerstoff in gasförmigem Zustand :: gaseous oxygen; gox
Scandium (Skandium) {n} »Sc« [chem.] :: scandium
Schwefel {m} »S« [chem.] :: sulphur; sulfur [Am.]; brimstone [former name]
 Schwefel in Stangen; Stangenschwefel {m} :: roll sulphur; roll brimstone [former name]
 eine Stange Schwefel :: a cane of sulphur
Selen {n} »Se« [chem.] :: selenium
Silicium {n} (Fachsprache); Silizium {n} »Si« [chem.] :: silicon
Silber {n} [chem.] »Ag« :: silver #BE:silver#
Stickstoff {m} »N« [chem.] :: nitrogen; azote [obs.]
 atmosphärischer Stickstoff; Stickstoff der Luft :: atmospheric nitrogen
 verfügbarer Stickstoff :: available nitrogen
 elementarer Stickstoff :: elemental nitrogen
 Flüssigstickstoff {m} :: liquid nitrogen »LN«
 gebundener Stickstoff :: bound nitrogen
 molekularer Stickstoff :: molecular nitrogen; dinitrogen
Strontium {n} »Sr« [chem.] :: strontium
Tantal {n} »Ta« [chem.] :: tantalum
Technetium {n} »Tc« [chem.] :: technetium
Tellur {n} »Te« [chem.] :: tellurium
Tenness {n} »Ts« [chem.] :: tennessine
Terbium {n} »Tb« [chem.] :: terbium
Thallium {n} »Tl« [chem.] :: thallium
Thorium {n} »Th« [chem.] :: thorium
Thulium {n} »Tm« [chem.] :: thulium
Zinn {n} »Sn« [chem.] :: tin #BE:tin# (stannum)
Titan {n} »Ti« [chem.] :: titanium
Unbibium {n} »Ubb« [chem.] :: unbibium
Uran {n} »U« [chem.] :: uranium
 angereichertes Uran :: enriched uranium
 natürliches Uran; Natururan {n} :: natural uranium
 waffenfähiges Uran :: weapons-grade uranium
Vanadium {n} »V« [chem.] :: vanadium
Wasserstoff {m} »H« [chem.] :: hydrogen
 flüssiger Wasserstoff »LH2« :: liquid hydrogen
 gasförmiger Wasserstoff :: gaseous hydrogen
 kohlenstoffhaltiger Wasserstoff :: carbonated hydrogen
Wolfram {n} »W« [chem.] :: tungsten; wolfram
Xenon {n} »Xe« [chem.] :: xenon
Ytterbium {n} »Yb« [chem.] :: ytterbium
Yttrium {n} »Y« [chem.] :: yttrium
Zink {n} »Zn« [chem.] :: zinc
Zirkonium {n}; Zirconium {n} »Zr« (oft fälschlich: Zirkon; Zircon) [chem.] :: zirconium
