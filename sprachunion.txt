Beinknochen {m} [anat.] :: leg bone
 Beinknochen {pl} :: leg bones
Geschwindigkeitsamplitude {f} [phys.] :: velocity amplitudes {pl}
Oberschenkelmuskulatur {f} [anat.] :: upper thigh muscles {pl}
Segmentkoordinatensystem {n} [sport] :: segment co-ordinate system
Markerposition {f} [med.] :: marker position
Wichtungsparameter {m} [phys.] :: weighting parameter
 Wichtungsparameter {pl} :: weighting parameters
Gelenkkinematik {f} [med.] :: joint kinematics {pl}
Formlinie {f} [geol.] :: form line
 Formlinien {pl} :: form lines
Höhenschichtenkarte {f} [geol.] :: hypsometric map
 Höhenschichtenkarten {pl} :: hypsometric maps
Skalierungsniveau {n} [geol.] :: scaling level
 Skalierungsniveaus {pl} :: scaling levels
Ikonizität {f} [ling.] :: iconicity
Farbenplastik {f} [geol.] :: color plastic
Betonverdichtung {f} [constr.] :: concrete compacting
Schalungshöhe {f} [constr.] :: formwork height
Knetbeutelverfahren {n} [constr.] :: kneading-bag test
Federzeichnung {f} :: ink drawing
 Federzeichnungen {pl} :: ink drawings
Peelingsalz {n} [min.] :: peeling salt
Säure-Basen-Haushalt {m} [biochem.] :: acid alkaline balance
Aquaristiksalz {n} [min.] :: aquarium salt(s) ({pl})
Meerwassersalzkonzentrat {n} [chem.] :: seawater salt concentrate
Fondsimmobilien {pl} [econ.] :: fund properties {pl}
Altstadtring {m} [arch.] :: old-town ring
Einzelhandelskaufhaus {n} [arch.] :: retail department store
 Einzelhandelskaufhäuser {pl} :: retail department stores
Kundenmagnet {m} [econ.] :: customer attraction
Glaspassage {f} [arch.] :: glass passageway
 Glaspassagen {pl} :: glases passageways
Straßennetzmodell {n} [arch.] :: road network model
 Straßennetzmodelle {pl} :: road network models
Verkehrsinformationssystem {n} [techn.] :: traffic information system
 Verkehrsinformationsysteme {pl} :: traffic information systems
Senatsverwaltung {f} [pol.] :: Senate Department
Softwareanbieter {m} [comp.] :: software provider
 Softwareanbieter {pl} :: software providers
Verpackungslogistik {f} [econ.] :: packaging logistics {pl}
Zollabwicklung {f} [econ.] :: customs handling
Prozessvisualisierung {f} [comp.] :: process visualization
Reederkommunikation {f} [econ.] :: shipowner communication
Fahrzeuglogistik {f} [econ.] :: automotive logistics {pl}
Belieferungskette {f} [econ.] :: supply chain
 Belieferungsketten {pl} :: supply chains
Heizungsart {f} [constr.] :: type of heating
 Heizungsarten {pl} :: types of heating
Wohnheimverwaltung {f} [stud.] :: accommodation office
Produktkatalog {m} [econ.] :: product catalogue
Teamintelligenz {f} [sport] :: team intelligence
Evaluationsinstrument {n} [sport] :: evaluation instrument
 Evaluationsinstrumente {pl} :: evaluation instruments
Sportpsychologie {f} [sport] :: sports psychology
Mannschaftsanalyse {f} [sport] :: team analysis
Abwehrenzym {n} [biochem.] :: defense enzyme
 Abwehrenzyme {pl} :: defense enzymes
Acetylradikal {n} [biochem.] :: acetyl radical
Akkumulationsgrad {m} :: degree of accumulation
Alarmschwelle {f} :: alert threshold
Alkoxylradikal {n} [biochem.] :: alcoxy radical
 Alkoxylradikale {pl} :: alcoxy radicals
Alkylamin {n} [biochem.] :: alkylamine
Alkylperoxiradikal {n} [biochem.] :: alkylperoxy radical
 Alkylperoxiradikale {pl} :: alkylperoxy radicals
Allelopathie {f} [biochem.] :: allelopathy
Alloenzym {n} [biochem.] :: alloenzyme
 Alloenzyme {pl} :: alloenzymes
Alphastrahlung {f} :: alpha rays
Aluminiumhypothese {f} :: aluminium hypothesis
Anabolismus {m} [biochem.] :: anabolism
Anreicherungsfaktor {m} [biochem.] :: accumulation factor
 Anreicherungsfaktoren {pl} :: accumulation factors
Anthocyanose {f} [biochem.] :: anthocyanosis
Aphizid {n} [biochem.] :: aphicide
Apoplast {m} [biochem.] :: apoplast
# Assimilationsdepression {f} [biochem.] :: reduction of assimilation
Assimilationsrate {f} [biochem.] :: assimilation rate
Atemgift {n} [biochem.] :: respiratory poison
 Atemgifte {pl} :: respiratory poisons
Atomabsorptionsspektroskopie {f} [chem.] :: atomic absorption spectrometry
Atomemissionsspektrometrie {f} [chem.] :: atomic emission spectrometry
Atrazin {n} [chem.] :: atrazine
Austauschkapazität {f} :: exchange capacity
Basenarmut {f} [chem.] :: base shortage
Basensättigung {f} [chem.] :: base saturation
Basensumme {f} [chem.] :: sum of bases
Begasungsdauer {f} [chem.] :: duration of exposure
Begrenzungswert {m} :: limit(ing) value
 Begrenzungswerte {pl} :: limit(ing) values
Beta-Absorption {f} [chem.] :: beta absorption
cytogenetisch {adj} [biochem.] :: cytogenetic
Bioindikatornetz {n} [biochem.] :: bioindicator grid
 Bioindikatornetze {pl} :: bioindicator grids
Biomasseproduktion {f} [envir.] :: biomass accumulation
Biomembran {f} [biochem.] :: biomembrane
 Biomembranen {pl} :: biomembranes
Biosonde {f} [biochem.] :: biosonde
 Biosonden {pl} :: biosondes
polychloriertes Biphenyl {n} [biochem.] :: polychlorinated biphenyl
Blattanalyse {f} [biochem.] :: foliar analysis
 Blattanalysen {pl} :: foliar analyses
Blattdeformation {f} [biochem.] :: leaf deformation
 Blattdeformationen {pl} :: leaf deformations
Blattgewichtsquotient {m} [bot.] :: leaf weight ratio
Blattinhaltsstoff {m} [bot.] :: leaf borne compound
Blattschädigung {f} [bot.] :: foliar injury
 Blattschädigungen {pl} :: foliar injuries
Bodenemission {f} [envir.] :: soil emission
Bodenenzym {n} [biochem.] :: soil enzyme
 Bodenenzyme {pl} :: soil enzymes
Bodennährstoff {m} [biochem.] :: soil nutrient
 Bodennährstoffe {pl} :: soil nutrients
chemischer Bodenparameter {m} [chem.] :: chemical soil parameter
Bodenversalzung {f} [envir.] :: soil salinization
Borkenanalyse {f} [agr.] :: bark analysis
BrOx-Zyklus {m} [envir.] :: BrOx-cycle
Buchensterben {n} [envir.] :: dieback of beech
Carbonylsulfid {n} [biochem.] :: carbon oxysulfide
Carotinoid {n} [biochem.] :: carotene
Catechin {n} [biochem.] :: catechin
Chloressigsäure {f} [chem.] :: chloroacetic acid
Chlorfluorkohlenwasserstoff {m} »CFKW«; Chlorfluorkohlenstoff »CFK« [chem.] [envir.] :: chlorofluorocarbon »CFC«
 Chlorfluorkohlenwasserstoff-Schmiermittel {n} :: chlorofluorocarbon oil
Chlorradikal {n} [biochem.] :: chlorine radical
Criegiee-Biradikal {n} [biochem.] :: Criegiee biradical
Cytokinin {n} [biochem.] :: cytokinin
Dieldrin {n} [biochem.] :: dieldrin
Disulfidbindung {f} [chem.] :: disulfide bond
Dithiocarbamate {pl} [biochem.] :: dithiocarbamates
Dürrling {m} [biol.] :: drought tree
Dunkelatmung {f} [bot.] :: dark respiration
Eichwert {m} :: calibration value
 Eichwerte {pl} :: calibration values
Eisbohrkern {m} :: ice core
 Eisbohrkerne {pl} :: ice cores
Emissionsfaktor {m} [envir.] :: emission factor
 Emissionsfaktoren {pl} :: emission factors
Emissionsgrad {m} [envir.] :: degree of emission
 Emissionsgrade {pl} :: degrees of emission
Emissionsmessung {f} [envir.] :: measurement of emissions
 Emissionsmessungen {pl} :: measurements of emissions
Emissionsminderung {f} [envir.] :: diminution of emissions
Emissionsüberwachung {f} [envir.] :: monitoring of emissions
Endosulfan {n} [biochem.] :: endosulfan
Enolase {f} [biochem.] :: enolase
Entgiftungsenzym {n} [biochem.] :: detoxifying enzyme
Entgiftungskapazität {f} [biol.] :: detoxification capacity
Esterase {f} [biochem.] :: esterase
 Esterasen {pl} :: esterases
Fahnenkrone {f} :: flag crown
Fichtensterben {n} [envir.] :: spruce dieback
Filterstack {m} [techn.] :: filter stack
Flammenionisation {f} [chem.] :: flame ionization
Flechtenkartierung {f} [envir.] :: lichen mapping
Flechtenwüste {f} [bot.] [envir.] :: lichen desert
 Flechtenwüsten {pl} :: lichen deserts
Flechtenzone {f} [bot.] [envir.] :: lichen zone
 Flechtenzonen {pl} :: lichen zones
Flugzeugabgas {n} [aviat.] :: aircraft exhaust
Flugzeugmessung {f} [aviat.] :: aircraft measurement
Fluorchlorkohlenwasserstoff {m} [chem.] :: chlorofluorocarbon
Fluorwasserstoff {m} [chem.] :: hydrogen fluoride
Forstschaden {m} [envir.] :: forest damage
Freiflächendeposition {f} [envir.] :: (open) field deposition
Frosttrocknis {f} [envir.] :: frost drought
Frühjahrsmaximum {n} :: spring maximum
Glutamat-Dehydrogenase {f} [biochem.] :: glutamate dehydrogenase
Gras- und Krüppelwaldzone {f} [envir.] :: grass and shrub-wood zone
Graskulturverfahren {n} [envir.] :: grass exposure method
bewährte Laboratoriumspraxis {f} [chem.] :: good laboratory practice
halbflüchtig {adj} [chem.] :: semivolatile
Hintergrundgebiet {n} :: background area
Histochemie {f} [biochem.] :: histochemistry
Höheninversion {f} :: free inversion
Hydroperoxyradikal {n} [biochem.] :: hydroxyperoxy radical
Hydroxylradikal {n} [biochem.] :: hydroxyl radical
Hydroxysulfonat {n} [chem.] :: hydroxysulfonate
Hyperakkumulation {f} [biochem.] :: hyperaccumulation
Hyperoxylradikal {n} [biochem.] :: hyperoxide radical
Immissionsberechnung {f} [envir.] :: simulation (calculation) of pollutant concentrations
Immissionsbeurteilung {f} [envir.] :: assessment of pollutant input
Immissionsdosis {f} [envir.] :: pollutant dose
Immissionseinwirkung {f} [envir.] :: impact of pollutants
Immissionsepisode {f} [envir.] :: pollution episode
Immissionsgebiet {n} [envir.] :: polluted area
 Immissionsgebiete {pl} :: polluted areas
Immissionsgefährdungszone {f} [envir.] :: area endangered by pollution
 Immissionsgefährdungszonen {pl} :: areas endangered by pollution
Immissionsgrenzkonzentration {f} [envir.] :: effect-related limit concentration
Immissionskartierung {f} [envir.] :: mapping of pollutant input
Immissionskonzentration {f} [envir.] :: pollutant concentration
Immissionsmessstation {f} [envir.] :: ambient air monitoring station
 Immissionsmessstationen {pl} :: ambient air monitoring stations
Immissionsüberwachung {f} [envir.] :: air pollutant monitoring
Immissionsmuster {n} [envir.] :: pattern of pollution
 Immissionsmuster {pl} :: patterns of pollution
Immissionsprognose {f} [envir.] :: air quality forecast
 Immissionsprognosen {pl} :: air quality forecasts
Immissionsrate {f} [envir.] :: rate of deposition; rate of pollution
Immissionsresistenz {f} [envir.] :: resistance to pollution
Monitororganismus {m} [biochem.] :: monitor organism
 Monitororganismen {pl} :: monitor organisms
Monofluoressigsäure {f} [chem.] :: monofluoracetic acid
Mykoplasma {f} [biochem.] :: mycoplasma
 Mykoplasmen {pl} :: mycoplasmas
Nadelbleichung {f} [bot.] :: needle blight
Nadeldichte {f} [bot.] :: needle density
Nadeljahrgang {m} [bot.] :: needle class
Nadelkrümmung {f} [bot.] :: needle curvature
Nadellänge {f} [bot.] :: needle length
Nadeloberfläche {f} [bot.] :: leaf area
Nadelquerschnittsfläche {f} [bot.] :: sectional area of the needle
Nadelspitzennekrose {f} [bot.] :: needle tip necrosis
Nadeltrockengewicht {n} [bot.] :: needle dry weight
Nadelverfärbung {f} [bot.] :: needle discolouration
Nadelverlustprozent {n} [bot.] :: percentage of needle loss
Nadelwachs {n} [bot.] :: needle wax
Nahemittent {n} [biochem.] :: nearby emitter
 Nahemittenten {pl} :: nearby emitters
Nahimmissionen {pl} [envir.] :: pollutions in the surrounding
Nährstoffverarmung {f} [biochem.] :: nutritive deficiency
Nekrobiose {f} [biochem.] :: necrobiosis
Nichtmethankohlenwasserstoff {m} [chem.] :: non methane hydrocarbon
Nitratatmung {f} [envir.] :: nitrate respiration
Nitratreduktase {f} [biochem.] :: nitrate reductase
Nitritreduktase {f} [biochem.] :: nitrite reductase
Nitrophenol {n} [chem.] :: nitrated phenol
Noxen {n} [envir.] :: noxes
Ökophysiologie {f} [envir.] :: ecophysiology
Ökosphäre {f} [envir.] :: ecosphere
Ombrograph {m} [biochem.] :: ombrograph
 Ombrographen {pl} :: ombrographs
Oxidase {f} [biochem.] :: oxidase
Oxidationskapazität {f} [envir.] :: oxidizing potential
Ozonbauch {m} [bot.] :: ozone belly
Ozonbildungspotenzial {n}; Ozonbildungspotential {n} [bot.] :: ozone formation potential
Ozonchemie {f} der Stratosphäre [chem.] :: ozone chemistry of the stratosphere
Ozonchemie {f} der Troposphäre [chem.] :: ozone chemistry of the troposphere
Ozoneinbruch {m} [bot.] :: ozone intrusion
Ozonid {n} [chem.] :: ozonide
Ozonjahresgänge {m} [bot.] :: course of ozone concentrations over the year
Ozonkerze {f} [biochem.] :: ozone candle
 Ozonkerzen {pl} :: ozone candles
Ozonsonde {f} [biochem.] :: ozone sonde
 Ozonsonden {pl} :: ozone sondes
Ozonzerstörungspotenzial {n}; Ozonzerstörungspotential {n} [bot.] :: ozone depleting potential
Palisadenzelle {f} [biochem.] :: palisade cell
 Palisadenzellen {pl} :: palisade cells
Parathion {n} [chem.] :: parathion
Passivsammler {m} [biochem.] :: passive sampler
 Passivsammler {pl} :: passive samplers
Pentachlorphenol {n} [chem.] :: pentachlorophenol
Perchlorethen {n} [chem.] :: perchloroethene
Peroxidation {f} [chem.] :: peroxidation
Peroxisom {n} [biochem.] :: peroxisom
Peroxyacetylnitrat {n} [chem.] :: peroxyacetyl nitrate
Peroxyacylnitrat {n} [chem.] :: peroxyacyl nitrate
Peroxy-Radikal {n} [biochem.] :: peroxy radical
Pestizidrückstand {m} [biochem.] :: pesticide residue
 Pestizidrückstände {pl} :: pesticide residues
Pflanzenanalyse {f} [biochem.] :: plant analysis
Pflanzenchemie {f} [chem.] :: phytochemistry
Pflanzeninhaltsstoff {m} [biochem.] :: plant constituent
 Pflanzeninhaltsstoffe {pl} :: plant constituents
Pflanzennährstoff {m} [biochem.] :: plant nutrient
 Pflanzennährstoffe {pl} :: plant nutrients
Pflanzenpigment {n} [biochem.] :: plant pigment
 Pflanzenpigmente {pl} :: plant pigments
Phenoloxidase {f} [biochem.] :: phenol oxidase
Phloemkollaps {m} [bot.] :: phloem collapse
Phosphorsäureester {m} [biochem.] :: phosphoric ester
Photolumineszenz {f} [biochem.] :: photoluminescence
Photooxidans {n} [biochem.] :: photooxidant
Photorespiration {f} [biochem.] :: photorespiration
Phytoalexin {n} [biochem.] :: phytoalexin
Phytochemie {f} [biochem.] :: phytochemistry
Phytoeffektor {m} [biochem.] :: phytoeffector
Phytohormon {n} [biochem.] :: phytohormone
Phytosphäre {f} [biochem.] :: phytosphere
Phytotoxizität {f} [biochem.] :: phytotoxicity
Phytotoxizitätstest {m} [biochem.] :: phytotoxicity test
Picein {n} [biochem.] :: picein
Pigmentbleichung {f} [bot.] :: pigment bleaching
Pinene {f} [biochem.] :: pinene
Pinosylvin {n} [biochem.] :: pinosylvin
Polyamin {n} [biochem.] :: polyamine
Protonenkonzentration {f} [phys.] :: proton concentration
Protonenquelle {f} [phys.] :: source of protons
 Protonenquellen {pl} :: sources of protons
Pufferbereich {m} [biochem.] :: buffer range
 Pufferbereiche {pl} :: buffer ranges
Pyradiometer {n} [biochem.] :: pyradiometer
Pyranometer {n} [biochem.] :: pyranometer
Pyrethroid {n} [biochem.] :: pyrethroid
