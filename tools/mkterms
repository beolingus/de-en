#!/usr/bin/perl

# 2006-05-15 fri: Holt aus de-en.txt fachspezifische Begriffe raus und
# erzeugt Wortlisten

use Getopt::Std;
getopts('a:fu', \%opts);

if ($#ARGV != 0) {
    print STDERR "Usage: $0 [-a abbrev-file] [-f] [-u] dict-file
   -a file with abbreviations
   -f include full entries (all forms, not just the [marked] lines
   -u dict-file is UTF-8
";
    exit;
}

$DICTFILE = $ARGV[0];

$full = $opts{'f'};

$CONV = $opts{'u'} ? '| iconv -f utf8 -t latin1 -' : '';

$DIR = '/afs/tu-chemnitz.de/project/beolingus/de-en/git';
$ABBFILE = $opts{'a'} ? $opts{'a'} : ($DIR . '/abb');
$LDIR = 'lists/';
$OUTDIR = `pwd`;
chomp($OUTDIR);
$OUTDIR .= '/';

$MIN = 1;      # mindestens so viele Spezialbegriffe, sonst ignoriert
$SPLITCOUNT = 50;  # wenn so viele Spezialbegriffe in einer Datei, f�ge
                   # Leerzeilen f�r jeden neuen Anfangsbuchstaben ein
#$MAX = 100;    # max. so viele Spezialbegriffe in einer Datei, sonst split

open(A, $ABBFILE) || die "Can't open $ABBFILE: $!\n";
$start = 0;
while (<A>) {
    chomp;
    next if (/^#/);
    push @ABB, $_;
    if ($start == 0) {
        next if (! /^Fachgebiete/);
        $start = 1;
        next;
    }
    chomp;
    ($de, $abb, $en, $ack) = split(/ :: /, $_, 4);
    next unless $abb;
    $subjects{$abb} = $de . ' :: ' . $en; 
    $ack{$abb} = $ack if $ack;
}
close(A);
$subjects{'[prov.]'} = 'Sprichw�rter :: proverbs';

# chdir $DIR;
# system('make $DICTFILE');

foreach $s (sort keys %subjects) {
    $count = 0;
    @out = ();
    print STDERR "$s: ";
    ($outfile = $s) =~ s/[.\[\]]//g;
    $outfile = $OUTDIR . $outfile;
    open(F, "fgrep '$s' $DICTFILE | sort $CONV |") or die "Can't start processes: $!\n";
    while (<F>) {
        chomp;
        if ($full) {
            s/\Q$s\E//g;
            s/<.*?>//g;
            push @out, $_;
            $count++;
        } else {
            # nur die "Zeile", die Muster enth�lt.
            my $l_de = $l_en = '';
            my ($de, $en) = split(/ :: /); 
            @DE = split(/\|/, $de);
            @EN = split(/\|/, $en);
            for ($i = 0; $i <= $#DE; $i++) {
                if ($DE[$i] =~ /\Q$s\E/ || $EN[$i] =~ /\Q$s\E/) {
                    $l_de .= '|' if ($l_de);
                    $l_de .= $DE[$i];
                    $l_en .= '|' if ($l_en);
                    $l_en .= $EN[$i];
                }
            }
            if ($l_de && $l_en) {
                $l_de =~ s/^ //;    # falls am anfang leer -> weg
                my $line = "$l_de :: $l_en";
                $line =~ s/ \Q$s\E//;
                $line =~ s/<>//g;
                push @out, $line;
                $count++;
            }
            #s/ \|.+(?= :: )//;     # erstes | bis vor ::
            #s/ \|.+//;
            #if (s/ \Q$s\E//) {
            #    push @out, $_;
            #    $count++;
            #}
        }
    }
    if ($count >= $MIN) {
        open(O, "> $outfile") || die "Can't open $outfile: $!\n";
        print O "$subjects{$s}\n";
        $ini_save = '';
        $lines = 0;
        foreach $l (@out) {
            $lines++;
            if ($count >= $SPLITCOUNT) {
                ($sl = $l) =~ s/^[( ]+//;
                $ini = uc(substr($sl, 0, 1));
                if ($ini ne $ini_save) {    # neuer Anfangsbuchstabe
                    #$legende .= '<a href="#' . $ini . '">' . $ini .'</a> &nbsp;&nbsp; ';
                    print O "\n";
                    $ini_save = $ini;
                }
            }
            # print O "$l\n";
            print O &ding2txt($l);
        }
        print O "_copyright :: $ack{$s}\n" if $ack{$s};
        close(O);
    }
    $abbcount{$s} = $count;
    close(F);
    print STDERR " $count\n";
}

$out = '';
foreach $l (@ABB) {
    if (! $l) {
        $out .= "\n";
        next;
    }
    if ($l eq ':') {
        $out .= "$l\n";
        next;
    }
    ($de, $abb, $en) = split(/ :: /, $l);
    $count = $abbcount{$abb} > 0 ? " ($abbcount{$abb})" : '';
    if ($abbcount{$abb} >= $MIN) {
        ($outfile = $abb) =~ s/[.\[\]]//g;
        $link = '<a href="' . $LDIR . $outfile . '.html">';
        $out .= $link . $de . "</a> :: " .
              $link . $abb . "</a>" . "$count :: " .
              $link . $en . "</a>\n";
    } else {
        $out .= "$de :: $abb$count :: $en\n";
    }
}
open (O, '>' . $OUTDIR . '00abb') || die "Can't open $OUTDIR/00abb: $!\n";
print O $out;
close O;

# de | de fortsetzung :: en | en fortsetzung 
# --> 
# de : en
#  de fortsetzung :: en fortsetzung

sub ding2txt {
    my($line) = @_;
    my $return = '';

    chomp($line);
    ($de, $en) = split(/ :: /, $line, 2);
    @DE = split(/\|/, $de);
    @EN = split(/\|/, $en);
    foreach (@DE) {
        s/ +$//;
        $e = shift(@EN); 
        $e =~ s/^ +//; 
        $e =~ s/ +$//;
        $return .= $_ . " :: " . $e . "\n";
    }
    return $return;
}
